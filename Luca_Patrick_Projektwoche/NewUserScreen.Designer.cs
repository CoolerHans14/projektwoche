using System.ComponentModel;

namespace Luca_Patrick_Projektwoche
{
    partial class NewUserScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbVorname = new System.Windows.Forms.TextBox();
            this.tbNachname = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbBenutzername = new System.Windows.Forms.TextBox();
            this.tbPasswort = new System.Windows.Forms.TextBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cbKeymember = new System.Windows.Forms.CheckBox();
            this.cbIButton = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vorname";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(155, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nachname";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(285, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "E-Mail";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(570, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Benutzername";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(708, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "Passwort";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(848, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "iButton";
            // 
            // tbVorname
            // 
            this.tbVorname.Location = new System.Drawing.Point(23, 73);
            this.tbVorname.Name = "tbVorname";
            this.tbVorname.Size = new System.Drawing.Size(91, 23);
            this.tbVorname.TabIndex = 6;
            // 
            // tbNachname
            // 
            this.tbNachname.Location = new System.Drawing.Point(155, 73);
            this.tbNachname.Name = "tbNachname";
            this.tbNachname.Size = new System.Drawing.Size(91, 23);
            this.tbNachname.TabIndex = 7;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(285, 73);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(91, 23);
            this.tbEmail.TabIndex = 8;
            // 
            // tbBenutzername
            // 
            this.tbBenutzername.Location = new System.Drawing.Point(570, 73);
            this.tbBenutzername.Name = "tbBenutzername";
            this.tbBenutzername.Size = new System.Drawing.Size(91, 23);
            this.tbBenutzername.TabIndex = 9;
            // 
            // tbPasswort
            // 
            this.tbPasswort.Location = new System.Drawing.Point(708, 73);
            this.tbPasswort.Name = "tbPasswort";
            this.tbPasswort.Size = new System.Drawing.Size(91, 23);
            this.tbPasswort.TabIndex = 10;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(415, 135);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(161, 37);
            this.btAdd.TabIndex = 12;
            this.btAdd.Text = "Hinzufügen";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(432, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 23);
            this.label7.TabIndex = 14;
            this.label7.Text = "Keymember";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // cbKeymember
            // 
            this.cbKeymember.Location = new System.Drawing.Point(460, 73);
            this.cbKeymember.Name = "cbKeymember";
            this.cbKeymember.Size = new System.Drawing.Size(104, 24);
            this.cbKeymember.TabIndex = 15;
            this.cbKeymember.UseVisualStyleBackColor = true;
            // 
            // cbIButton
            // 
            this.cbIButton.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIButton.FormattingEnabled = true;
            this.cbIButton.Location = new System.Drawing.Point(848, 73);
            this.cbIButton.Name = "cbIButton";
            this.cbIButton.Size = new System.Drawing.Size(98, 23);
            this.cbIButton.TabIndex = 16;
            // 
            // NewUserScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 195);
            this.Controls.Add(this.cbIButton);
            this.Controls.Add(this.cbKeymember);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.tbPasswort);
            this.Controls.Add(this.tbBenutzername);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbNachname);
            this.Controls.Add(this.tbVorname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewUserScreen";
            this.Text = "NewUserScreen";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.TextBox tbPasswort;
        private System.Windows.Forms.TextBox tbBenutzername;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbNachname;
        private System.Windows.Forms.TextBox tbVorname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbKeymember;
        private System.Windows.Forms.ComboBox cbIButton;
    }
}