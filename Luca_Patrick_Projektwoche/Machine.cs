using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WinDruckauftrag;

namespace Luca_Patrick_Projektwoche
{
    public class Machine
    {
        
        public string MaschinenID { get; private set; }
        public Dbase Database { get; private set; }

        public bool isOn { get; set; }

        private Table LogTable;

        public Machine(string MachinenID, Dbase database)
        {
            this.MaschinenID = MachinenID;
            Database = database;
            isOn = false;
            LogTable = new Table(database, "log");
        }

        public bool StartMaschine(string iButton)
        {
            if (!Database.ProofDataWithContition("zuweisung","iButtonID"," where zuweisung.iButtonID like '" + iButton + "' and zuweisung.MaschinenID like '" + MaschinenID+"'"))
            {
                return false;
            }
            else
            {
                isOn = true;
                string[] array = new string[5];
                array[0] = string.Empty;
                array[1] = iButton;
                array[2] = MaschinenID;
                array[3] = DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" +
                           DateTime.Now.Day.ToString("00") + " " + DateTime.Now.Hour.ToString("00") + ":" +
                           DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
                array[4] = string.Empty;
                LogTable.InsertData(array);
                return true;
            }
        }

        public bool StopMaschine(string iButton)
        {
            List<string[]> result = Database.CommandSelectAsListFrom("LogID", "log", " where iButtonID like '" + iButton+"' order by 1 DESC LIMIT 1");
            if (result.Count <= 0)
            {
                return false;
            }

            isOn = false;
            LogTable.UpdateLine(result[0][0],"LogID","Endtime",DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" +
                                                               DateTime.Now.Day.ToString("00") + " " + DateTime.Now.Hour.ToString("00") + ":" +
                                                               DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00"));
            return true;
        }
    }
}