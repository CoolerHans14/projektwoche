using System;
using System.Windows.Forms;
using WinDruckauftrag;


namespace Luca_Patrick_Projektwoche
{
    public partial class NewMaschineScreen : Form
    {
        public event AddToDatabase InsertIntoDatabase;
        private Dbase Database;
        public NewMaschineScreen(AddToDatabase function, Dbase database)
        {
            InitializeComponent();
            InsertIntoDatabase += function;
            Database = database;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            string[] array = new string[2];
            array[0] = tbID.Text;
            array[1] = tbBezeichnung.Text;
            if (!Database.ProofData(tbID.Text, "MaschinenID", "maschine"))
            {
                if (InsertIntoDatabase != null) InsertIntoDatabase(array);
                this.Close();
            }
            else
            {
                MessageBox.Show("Fehler: Maschine bereits vorhanden");
            }
        }
    }
}