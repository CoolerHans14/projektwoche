using System.ComponentModel;

namespace Luca_Patrick_Projektwoche
{
    partial class NewAllocationScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbButtonID = new System.Windows.Forms.ComboBox();
            this.cbMaschineID = new System.Windows.Forms.ComboBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(73, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "ButtonID";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(259, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "MaschinenID";
            // 
            // cbButtonID
            // 
            this.cbButtonID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbButtonID.FormattingEnabled = true;
            this.cbButtonID.Location = new System.Drawing.Point(37, 65);
            this.cbButtonID.Name = "cbButtonID";
            this.cbButtonID.Size = new System.Drawing.Size(111, 23);
            this.cbButtonID.TabIndex = 2;
            // 
            // cbMaschineID
            // 
            this.cbMaschineID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaschineID.FormattingEnabled = true;
            this.cbMaschineID.Location = new System.Drawing.Point(218, 65);
            this.cbMaschineID.Name = "cbMaschineID";
            this.cbMaschineID.Size = new System.Drawing.Size(111, 23);
            this.cbMaschineID.TabIndex = 3;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(126, 133);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(139, 24);
            this.btAdd.TabIndex = 4;
            this.btAdd.Text = "Hinzufügen";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // NewAllocationScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 189);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.cbMaschineID);
            this.Controls.Add(this.cbButtonID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewAllocationScreen";
            this.Text = "NewAllocationScreen";
            this.Load += new System.EventHandler(this.NewAllocationScreen_Load);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbMaschineID;
        private System.Windows.Forms.ComboBox cbButtonID;
    }
}