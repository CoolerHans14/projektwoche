using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Luca_Patrick_Projektwoche
{
    public delegate void AddToDatabase(string[] values);
    public partial class NewUserScreen : Form
    {
        public event AddToDatabase InsertInDatabase;

        public NewUserScreen(AddToDatabase function, List<string[]> iButtons)
        {
            InitializeComponent();
            InsertInDatabase += function;
            foreach (var button in iButtons)
            {
                cbIButton.Items.Add(button[0]);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            string[] array = new string[8];
            array[0] = string.Empty;
            array[1] = tbVorname.Text;
            array[2] = tbNachname.Text;
            array[3] = tbEmail.Text;
            array[4] = cbKeymember.Checked ? "1" : "0";
            array[5] = tbBenutzername.Text;
            array[6] = tbPasswort.Text;
            array[7] = cbIButton.Text;
            if (InsertInDatabase != null) InsertInDatabase(array);
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {
        }
    }
}