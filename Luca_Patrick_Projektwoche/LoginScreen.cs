using System;
using System.Windows.Forms;
using WinDruckauftrag;

namespace Luca_Patrick_Projektwoche
{
    public partial class LoginScreen : Form
    {
        private Form parentWindow;
        private Dbase database;
        public LoginScreen(Form parent, Dbase database)
        {
            parentWindow = parent;
            parentWindow.Enabled = false;
            this.database = database;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btAbbruch_Click_1(object sender, EventArgs e)
        {
            parentWindow.Close();
            this.Close();
        }

        private void btLogin_Click_1(object sender, EventArgs e)
        {
            var data = database.CommandSelectAsListFrom("Vorname, Nachname", "user", "where Benutzername = '" + tbName.Text + "' and Passwort = '" + tbPasswort.Text + "' and Keymember = 1");
            if (data.Count <= 0)
            {
                lbError.Text = "Keine Berechtigung oder falsche eingabe";
            }
            else
            {
                parentWindow.Enabled = true;
                this.Close();
            }
        }
    }
}