using System.ComponentModel;
using WinZugriffssteuerung;

namespace Luca_Patrick_Projektwoche
{
    public delegate void AfterKeyRead();

    public delegate void WhileKeyRead(string button);

    public delegate string ReadKeyID();
    public class KeyReader
    {
        public event AfterKeyRead OnKeyReadDone; //funktion die aufgerufen wird wenn der backroundworker ein key erkennt
        public event WhileKeyRead OnKeyRead;     //funktion die aufgerufen wird wenn der backroundworker fertig ist
        public event ReadKeyID ReadID;  //funktion die ein key zurückgibt (in der Regel iButton.read_ID())
        public KeyReader()
        {
        }

        public void ReadKey()
        {
            BackgroundWorker back = new BackgroundWorker();
            back.DoWork += (x, y) =>
            {

                // button.serialinit(); 
                if (ReadID != null)
                {
                    string data = ReadID();
                    if (data != null)
                    {
                        if (OnKeyRead != null) OnKeyRead(data);
                    }
                }
            };
            back.RunWorkerCompleted += (x, y) =>
            {
                if (OnKeyReadDone != null) OnKeyReadDone();
            };
            back.RunWorkerAsync();
        }
    }
}