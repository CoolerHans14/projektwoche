using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WinDruckauftrag;

namespace Luca_Patrick_Projektwoche
{
    public partial class NewAllocationScreen : Form
    {

        public event AddToDatabase InsertIntoDatabase;
        private Dbase Database;
        public NewAllocationScreen(AddToDatabase function, List<string[]> buttons, List<string[]> maschines, Dbase database)
        {
            InitializeComponent();
            Database = database;
            InsertIntoDatabase += function;
            foreach (var button in buttons)
            {
                cbButtonID.Items.Add(button[0]);
            }

            foreach (var maschine in maschines)
            {
                cbMaschineID.Items.Add(maschine[0]);
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            String[] array = new string[3];
            array[0] = cbButtonID.Text;
            array[1] = cbMaschineID.Text;
            if (!Database.ProofDataWithAnd(cbButtonID.Text,cbMaschineID.Text,"MaschinenID", "IButtonID", "zuweisung")){
                array[2] = DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00");
                if (InsertIntoDatabase != null) InsertIntoDatabase(array);
                this.Close();
            }
            else
            {
                MessageBox.Show("Fehler: Eintrag bereits vorhanden");
            }
            
        }

        private void NewAllocationScreen_Load(object sender, EventArgs e)
        {
        }
    }
}