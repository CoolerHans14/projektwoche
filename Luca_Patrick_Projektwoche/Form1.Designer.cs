﻿namespace Luca_Patrick_Projektwoche
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.UserPage = new System.Windows.Forms.TabPage();
            this.btDeleteUser = new System.Windows.Forms.Button();
            this.btNewUser = new System.Windows.Forms.Button();
            this.lUserPageMessage = new System.Windows.Forms.Label();
            this.dgUser = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btKeyEintragen = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btKeyLesen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.MaschinPage = new System.Windows.Forms.TabPage();
            this.btDeleteMaschine = new System.Windows.Forms.Button();
            this.btNewMaschine = new System.Windows.Forms.Button();
            this.MaschinenMessage = new System.Windows.Forms.Label();
            this.dgMaschine = new System.Windows.Forms.DataGridView();
            this.allocationPage = new System.Windows.Forms.TabPage();
            this.btDeleteAllocation = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgAllocation = new System.Windows.Forms.DataGridView();
            this.simulationPage = new System.Windows.Forms.TabPage();
            this.lSimulationsResult = new System.Windows.Forms.Label();
            this.btStartSimulation = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMaschine = new System.Windows.Forms.ComboBox();
            this.LogPage = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.dgLog = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.UserPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUser)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.MaschinPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMaschine)).BeginInit();
            this.allocationPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllocation)).BeginInit();
            this.simulationPage.SuspendLayout();
            this.LogPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
            this.SuspendLayout();
            // 
            // UserPage
            // 
            this.UserPage.Controls.Add(this.btDeleteUser);
            this.UserPage.Controls.Add(this.btNewUser);
            this.UserPage.Controls.Add(this.lUserPageMessage);
            this.UserPage.Controls.Add(this.dgUser);
            this.UserPage.Location = new System.Drawing.Point(4, 25);
            this.UserPage.Margin = new System.Windows.Forms.Padding(2);
            this.UserPage.Name = "UserPage";
            this.UserPage.Padding = new System.Windows.Forms.Padding(2);
            this.UserPage.Size = new System.Drawing.Size(1739, 710);
            this.UserPage.TabIndex = 1;
            this.UserPage.Text = "User";
            this.UserPage.UseVisualStyleBackColor = true;
            this.UserPage.Paint += new System.Windows.Forms.PaintEventHandler(this.UserPage_Paint);
            // 
            // btDeleteUser
            // 
            this.btDeleteUser.Location = new System.Drawing.Point(1230, 21);
            this.btDeleteUser.Name = "btDeleteUser";
            this.btDeleteUser.Size = new System.Drawing.Size(298, 31);
            this.btDeleteUser.TabIndex = 3;
            this.btDeleteUser.Text = "Ausgewählte Zeile Löschen";
            this.btDeleteUser.UseVisualStyleBackColor = true;
            this.btDeleteUser.Click += new System.EventHandler(this.btDeleteUser_Click);
            // 
            // btNewUser
            // 
            this.btNewUser.Location = new System.Drawing.Point(1587, 21);
            this.btNewUser.Name = "btNewUser";
            this.btNewUser.Size = new System.Drawing.Size(118, 27);
            this.btNewUser.TabIndex = 2;
            this.btNewUser.Text = "Neuer User";
            this.btNewUser.UseVisualStyleBackColor = true;
            this.btNewUser.Click += new System.EventHandler(this.btNewUser_Click);
            // 
            // lUserPageMessage
            // 
            this.lUserPageMessage.Location = new System.Drawing.Point(118, 13);
            this.lUserPageMessage.Name = "lUserPageMessage";
            this.lUserPageMessage.Size = new System.Drawing.Size(555, 35);
            this.lUserPageMessage.TabIndex = 1;
            this.lUserPageMessage.Text = "Änderungen in der Table ändern die Datenbank";
            this.lUserPageMessage.Click += new System.EventHandler(this.label3_Click);
            // 
            // dgUser
            // 
            this.dgUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUser.Location = new System.Drawing.Point(17, 74);
            this.dgUser.Name = "dgUser";
            this.dgUser.Size = new System.Drawing.Size(1699, 631);
            this.dgUser.TabIndex = 0;
            this.dgUser.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUser_CellValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btKeyEintragen);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btKeyLesen);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(1739, 710);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Auslesen";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btKeyEintragen
            // 
            this.btKeyEintragen.Location = new System.Drawing.Point(858, 429);
            this.btKeyEintragen.Name = "btKeyEintragen";
            this.btKeyEintragen.Size = new System.Drawing.Size(131, 50);
            this.btKeyEintragen.TabIndex = 3;
            this.btKeyEintragen.Text = "Key Eintragen";
            this.btKeyEintragen.UseVisualStyleBackColor = true;
            this.btKeyEintragen.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(902, 354);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // btKeyLesen
            // 
            this.btKeyLesen.Location = new System.Drawing.Point(665, 429);
            this.btKeyLesen.Margin = new System.Windows.Forms.Padding(2);
            this.btKeyLesen.Name = "btKeyLesen";
            this.btKeyLesen.Size = new System.Drawing.Size(131, 50);
            this.btKeyLesen.TabIndex = 1;
            this.btKeyLesen.Text = "Key Lesen";
            this.btKeyLesen.UseVisualStyleBackColor = true;
            this.btKeyLesen.Click += new System.EventHandler(this.btKeyLesen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(707, 354);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.tabPage1);
            this.TabControl.Controls.Add(this.UserPage);
            this.TabControl.Controls.Add(this.MaschinPage);
            this.TabControl.Controls.Add(this.allocationPage);
            this.TabControl.Controls.Add(this.simulationPage);
            this.TabControl.Controls.Add(this.LogPage);
            this.TabControl.Location = new System.Drawing.Point(0, 1);
            this.TabControl.Margin = new System.Windows.Forms.Padding(2);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1747, 739);
            this.TabControl.TabIndex = 1;
            // 
            // MaschinPage
            // 
            this.MaschinPage.Controls.Add(this.btDeleteMaschine);
            this.MaschinPage.Controls.Add(this.btNewMaschine);
            this.MaschinPage.Controls.Add(this.MaschinenMessage);
            this.MaschinPage.Controls.Add(this.dgMaschine);
            this.MaschinPage.Location = new System.Drawing.Point(4, 25);
            this.MaschinPage.Name = "MaschinPage";
            this.MaschinPage.Padding = new System.Windows.Forms.Padding(3);
            this.MaschinPage.Size = new System.Drawing.Size(1739, 710);
            this.MaschinPage.TabIndex = 2;
            this.MaschinPage.Text = "Maschinen";
            this.MaschinPage.UseVisualStyleBackColor = true;
            this.MaschinPage.Paint += new System.Windows.Forms.PaintEventHandler(this.MaschinPage_Paint);
            // 
            // btDeleteMaschine
            // 
            this.btDeleteMaschine.Location = new System.Drawing.Point(1171, 29);
            this.btDeleteMaschine.Name = "btDeleteMaschine";
            this.btDeleteMaschine.Size = new System.Drawing.Size(283, 26);
            this.btDeleteMaschine.TabIndex = 3;
            this.btDeleteMaschine.Text = "Ausgewählte Zeile Löschen";
            this.btDeleteMaschine.UseVisualStyleBackColor = true;
            this.btDeleteMaschine.Click += new System.EventHandler(this.btDeleteMaschine_Click);
            // 
            // btNewMaschine
            // 
            this.btNewMaschine.Location = new System.Drawing.Point(1486, 29);
            this.btNewMaschine.Name = "btNewMaschine";
            this.btNewMaschine.Size = new System.Drawing.Size(230, 26);
            this.btNewMaschine.TabIndex = 2;
            this.btNewMaschine.Text = "Neue Maschine";
            this.btNewMaschine.UseVisualStyleBackColor = true;
            this.btNewMaschine.Click += new System.EventHandler(this.btNewMaschine_Click);
            // 
            // MaschinenMessage
            // 
            this.MaschinenMessage.Location = new System.Drawing.Point(297, 21);
            this.MaschinenMessage.Name = "MaschinenMessage";
            this.MaschinenMessage.Size = new System.Drawing.Size(1073, 34);
            this.MaschinenMessage.TabIndex = 1;
            this.MaschinenMessage.Text = "Änderungen in der Anzeige werden in der Datenbank übernommen";
            // 
            // dgMaschine
            // 
            this.dgMaschine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMaschine.Location = new System.Drawing.Point(17, 71);
            this.dgMaschine.Name = "dgMaschine";
            this.dgMaschine.Size = new System.Drawing.Size(1712, 633);
            this.dgMaschine.TabIndex = 0;
            this.dgMaschine.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMaschine_CellValueChanged);
            this.dgMaschine.Paint += new System.Windows.Forms.PaintEventHandler(this.dataGridView1_Paint);
            // 
            // allocationPage
            // 
            this.allocationPage.Controls.Add(this.btDeleteAllocation);
            this.allocationPage.Controls.Add(this.button1);
            this.allocationPage.Controls.Add(this.dgAllocation);
            this.allocationPage.Location = new System.Drawing.Point(4, 25);
            this.allocationPage.Name = "allocationPage";
            this.allocationPage.Padding = new System.Windows.Forms.Padding(3);
            this.allocationPage.Size = new System.Drawing.Size(1739, 710);
            this.allocationPage.TabIndex = 3;
            this.allocationPage.Text = "Zuweisung";
            this.allocationPage.UseVisualStyleBackColor = true;
            this.allocationPage.Paint += new System.Windows.Forms.PaintEventHandler(this.allocationPage_Paint);
            // 
            // btDeleteAllocation
            // 
            this.btDeleteAllocation.Location = new System.Drawing.Point(1210, 33);
            this.btDeleteAllocation.Name = "btDeleteAllocation";
            this.btDeleteAllocation.Size = new System.Drawing.Size(256, 26);
            this.btDeleteAllocation.TabIndex = 2;
            this.btDeleteAllocation.Text = "Ausgewählte Zeile Löschen";
            this.btDeleteAllocation.UseVisualStyleBackColor = true;
            this.btDeleteAllocation.Click += new System.EventHandler(this.btDeleteAllocation_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1490, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(211, 26);
            this.button1.TabIndex = 1;
            this.button1.Text = "Neue Zuweisung";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dgAllocation
            // 
            this.dgAllocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAllocation.Location = new System.Drawing.Point(7, 85);
            this.dgAllocation.Name = "dgAllocation";
            this.dgAllocation.Size = new System.Drawing.Size(1722, 619);
            this.dgAllocation.TabIndex = 0;
            this.dgAllocation.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAllocation_CellValueChanged);
            // 
            // simulationPage
            // 
            this.simulationPage.Controls.Add(this.lSimulationsResult);
            this.simulationPage.Controls.Add(this.btStartSimulation);
            this.simulationPage.Controls.Add(this.label3);
            this.simulationPage.Controls.Add(this.cbMaschine);
            this.simulationPage.Location = new System.Drawing.Point(4, 25);
            this.simulationPage.Name = "simulationPage";
            this.simulationPage.Padding = new System.Windows.Forms.Padding(3);
            this.simulationPage.Size = new System.Drawing.Size(1739, 710);
            this.simulationPage.TabIndex = 4;
            this.simulationPage.Text = "Simulation";
            this.simulationPage.UseVisualStyleBackColor = true;
            this.simulationPage.Paint += new System.Windows.Forms.PaintEventHandler(this.simulationPage_Paint);
            // 
            // lSimulationsResult
            // 
            this.lSimulationsResult.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lSimulationsResult.Location = new System.Drawing.Point(211, 226);
            this.lSimulationsResult.Name = "lSimulationsResult";
            this.lSimulationsResult.Size = new System.Drawing.Size(1483, 243);
            this.lSimulationsResult.TabIndex = 3;
            // 
            // btStartSimulation
            // 
            this.btStartSimulation.Location = new System.Drawing.Point(718, 125);
            this.btStartSimulation.Name = "btStartSimulation";
            this.btStartSimulation.Size = new System.Drawing.Size(331, 43);
            this.btStartSimulation.TabIndex = 2;
            this.btStartSimulation.Text = "Simulation Starten/Stoppen";
            this.btStartSimulation.UseVisualStyleBackColor = true;
            this.btStartSimulation.Click += new System.EventHandler(this.btStartSimulation_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(703, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(374, 29);
            this.label3.TabIndex = 1;
            this.label3.Text = "Geben Sie an Welche Maschine sie Simulieren möchten";
            // 
            // cbMaschine
            // 
            this.cbMaschine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaschine.FormattingEnabled = true;
            this.cbMaschine.Location = new System.Drawing.Point(747, 82);
            this.cbMaschine.Name = "cbMaschine";
            this.cbMaschine.Size = new System.Drawing.Size(263, 24);
            this.cbMaschine.TabIndex = 0;
            // 
            // LogPage
            // 
            this.LogPage.Controls.Add(this.label4);
            this.LogPage.Controls.Add(this.dgLog);
            this.LogPage.Location = new System.Drawing.Point(4, 25);
            this.LogPage.Name = "LogPage";
            this.LogPage.Padding = new System.Windows.Forms.Padding(3);
            this.LogPage.Size = new System.Drawing.Size(1739, 710);
            this.LogPage.TabIndex = 5;
            this.LogPage.Text = "Log";
            this.LogPage.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(163, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1489, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "Änderungen am Log sind aus sicherheitsgründen verboten";
            // 
            // dgLog
            // 
            this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLog.Location = new System.Drawing.Point(9, 86);
            this.dgLog.Name = "dgLog";
            this.dgLog.Size = new System.Drawing.Size(1719, 617);
            this.dgLog.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1603, 747);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 31);
            this.button2.TabIndex = 2;
            this.button2.Text = "Aktualisieren";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1747, 779);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Verwaltung";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.UserPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUser)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.TabControl.ResumeLayout(false);
            this.MaschinPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMaschine)).EndInit();
            this.allocationPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAllocation)).EndInit();
            this.simulationPage.ResumeLayout(false);
            this.LogPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage UserPage;
        private System.Windows.Forms.Button btNewUser;
        private System.Windows.Forms.Label lUserPageMessage;
        private System.Windows.Forms.DataGridView dgUser;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btKeyLesen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btKeyEintragen;
        private System.Windows.Forms.TabPage MaschinPage;
        private System.Windows.Forms.Label MaschinenMessage;
        private System.Windows.Forms.Button btNewMaschine;
        private System.Windows.Forms.DataGridView dgMaschine;
        private System.Windows.Forms.TabPage allocationPage;
        private System.Windows.Forms.DataGridView dgAllocation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage simulationPage;
        private System.Windows.Forms.Button btStartSimulation;
        private System.Windows.Forms.Label lSimulationsResult;
        private System.Windows.Forms.ComboBox cbMaschine;
        private System.Windows.Forms.Button btDeleteUser;
        private System.Windows.Forms.Button btDeleteMaschine;
        private System.Windows.Forms.Button btDeleteAllocation;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage LogPage;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgLog;
    }
}