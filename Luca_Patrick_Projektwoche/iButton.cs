﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Windows.Forms;

namespace WinZugriffssteuerung
{
    public class iButton
    {
        private SerialPort serport; //SerialPort-Object
        string com;
        //Needed variables
        private string ids = string.Empty;
        // private string com = "COM6"; //Used COM-Port                               

        public string read_IDs()
        {

            //Only for test if the data reading works fine
            try
            {
                //serport.Open(); //Opens the serial-connection

                //ids = serport.ReadLine(); //Reads the data from the serial connection                
                //serport.Close();


                ids = "C08B241700574100;pds231"; //zum Testen
                if (ids != null) //Check if the data stored in IDs is a valid 
                    return ids; //Output of the valid IDs
                else
                    MessageBox.Show("IDs fehlerhaft!"); //Error if the data in "mac" is not a valid MAC-Address

                ids = "";

                //Closing the serial-connection
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error"); //Shows an error if there is something wrong with the serial connection

                serport.Close(); //Closing the serial-connection
            }

            return "kek";
        }

        public void serialinit()
        {
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"] != null)
                    {
                        if (queryObj["Caption"].ToString().Contains("(COM"))
                        {
                           // MessageBox.Show("serial port :" + queryObj["Caption"]);
                            if (queryObj["Caption"].ToString().StartsWith("USB-SERIAL CH340"))
                            {
                                com =  queryObj["Caption"].ToString();
                                com = com.Split('(')[1].TrimEnd(')');
                            }
                        }
                    }
                }
            }
            catch (ManagementException e)
            {
                MessageBox.Show(e.Message);

            }
           
                serport = new SerialPort(com); //New instance of the SerialPort-Class with the used COM-Interface as transfer parameter
                serport.BaudRate = 115200; //Setting the BaudRate to 9600
                serport.Parity = Parity.None; //Setting Parity to None
                serport.DataBits = 8; //Used Bits for data is set to 8
                serport.StopBits = StopBits.One; //StopBit is set to 1
            
        }
    }

}

