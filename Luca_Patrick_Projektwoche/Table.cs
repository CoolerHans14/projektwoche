using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using WinDruckauftrag;

namespace Luca_Patrick_Projektwoche
{
    public class Table
    {
        private Dbase Database;
        private string TableString;
        
        public Table(Dbase database, string table)
        {
            Database = database;
            TableString = table;
        }

        public DataTable GetData()
        {
            return Database.CommandSelectAsDataTableFrom(TableString);
        }

        public void DeleteData(string columnName, string value)
        {
            string condition = columnName + " = '" + value +"'";
            Database.DeleteData(TableString, condition);
        }
        
        public void DeleteData(string condition)
        {
            Database.DeleteData(TableString, condition);
        }

        public bool ProofData(string proof, string column)
        {
           return Database.ProofData(proof,column,TableString);
        }

        public void UpdateLine(string row, string rowName, string columnName, string value)
        {
            string condition = " where " + rowName + " = '" + row + "'";
            string set = " " + "`" + columnName + "`" + " = " + "'"+ value+ "'";
            Database.CommandUpdate(TableString, set,condition);
        }
        
        public void InsertData(string[] values)
        {
            
            List<string> columnNames = new List<string>();
            DataTable data = GetData();
            for (int i = 0; i < data.Columns.Count;i++)
            {
                columnNames.Add("`"+data.Columns[i].ColumnName+"`");
            }
            
            Database.CommandInsertInto(TableString, columnNames.ToArray(), values);
        }
    }
}