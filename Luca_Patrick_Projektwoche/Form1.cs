﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinZugriffssteuerung;
using WinDruckauftrag;

namespace Luca_Patrick_Projektwoche
{
    public partial class Form1 : Form
    {
        iButton button = new iButton();
        Dbase database;
        private Table UserTabel;
        private Table ButtonTable;
        private Table MaschinTable;
        private Table AllocationTable;
        private Table LogTable;
        private Machine Maschine;
        private bool isRunning = false;

        public Form1()
        {
            InitializeComponent();
            database = new Dbase("projektlabor", "root", "");
            UserTabel = new Table(database, "user");
            ButtonTable = new Table(database, "ibutton");
            MaschinTable = new Table(database, "maschine");
            AllocationTable = new Table(database, "zuweisung");
            LogTable = new Table(database, "log");
            btKeyEintragen.Enabled = false;
            Form login = new LoginScreen(this, database);
            login.ShowDialog();
            label1.Text = string.Empty;
            label2.Text = string.Empty;
            InitDataGridViews();
        }

        private void InitDataGridViews()
        {
            dgUser.DataSource = UserTabel.GetData().DefaultView;
            dgUser.Columns[0].ReadOnly = true;

            dgMaschine.DataSource = MaschinTable.GetData().DefaultView;
            dgMaschine.Columns[0].ReadOnly = true;

            dgAllocation.DataSource = AllocationTable.GetData();

            dgLog.DataSource = LogTable.GetData();
            dgLog.ReadOnly = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<string[]> Maschinen = database.CommandSelectAsListFrom("MaschinenID", "maschine", "");
            foreach (var maschine in Maschinen)
            {
                cbMaschine.Items.Add(maschine[0]);
            }
        }

        private void btKeyLesen_Click(object sender, EventArgs e)
        {
            //krasse event sachen
            btKeyLesen.Enabled = false;
            KeyReader reader = new KeyReader();
            reader.ReadID += button.read_IDs;
            reader.OnKeyRead += changeAfterKeyRead;
            reader.OnKeyReadDone += keyReadDone;
            reader.ReadKey();
        }

        private void changeAfterKeyRead(string key)
        {
            var tmp = key.Split(';');
            label1.Invoke(new Action(() => { label1.Text = tmp[1]; }));
            label2.Invoke(new Action(() => { label2.Text = tmp[0]; }));
        }

        private void keyReadDone()
        {
            btKeyLesen.Enabled = true;
            btKeyEintragen.Enabled = true;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
        }

        private void UserPage_Paint(object sender, PaintEventArgs e)
        {
        }

        private void dgUser_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgUser.Columns[e.ColumnIndex].Name.ToString() == "iButtonID")
            {
                if (!database.ProofData(dgUser[e.ColumnIndex, e.RowIndex].Value.ToString(),
                    dgUser.Columns[e.ColumnIndex].Name.ToString(), "ibutton"))
                {
                    lUserPageMessage.Text = "Der Button exestiert nicht";
                    return;
                }

                if (database.ProofData(dgUser[e.ColumnIndex, e.RowIndex].Value.ToString(),
                    dgUser.Columns[e.ColumnIndex].Name.ToString(), "user"))
                {
                    lUserPageMessage.Text = "Der button wurde einem anderen gegeben";
                    return;
                }
            }

            string value;
            if (dgUser[e.ColumnIndex, e.RowIndex].Value.ToString() == "True")
            {
                value = "1";
            }
            else if (dgUser[e.ColumnIndex, e.RowIndex].Value.ToString() == "False")
            {
                value = "0";
            }
            else
            {
                value = dgUser[e.ColumnIndex, e.RowIndex].Value.ToString();
            }

            UserTabel.UpdateLine(dgUser[0, e.RowIndex].Value.ToString(), dgUser.Columns[0].Name,
                dgUser.Columns[e.ColumnIndex].Name, value);
            lUserPageMessage.Text = "Änderung erfolgreich";
            InitDataGridViews();
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void btNewUser_Click(object sender, EventArgs e)
        {
            var buttonList = database.CommandSelectAsListFrom("iButtonID", "ibutton",
                " where ibutton.iButtonID not in (Select user.iButtonID from user)");
            Form addForm = new NewUserScreen(UserTabel.InsertData, buttonList);
            addForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] datas = new string[]
            {
                label2.Text,
                "DS1990"
            };
            if (label2.Text != null)
            {
                //List <string[]> s = database.CommandSelectAsListFrom("ibutton", "ibuttonid");


                //ButtonTable.InsertData(datas);
                //btKeyEintragen.Enabled = false;
                if (database.ProofData(label2.Text, "ibuttoniD", "ibutton") == false)
                {
                    ButtonTable.InsertData(datas);
                    btKeyEintragen.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Eintrag bereits vorhanden");
                }
            }
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void MaschinPage_Paint(object sender, PaintEventArgs e)
        {
        }

        private void dgMaschine_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MaschinTable.UpdateLine(dgMaschine[0, e.RowIndex].Value.ToString(), dgMaschine.Columns[0].Name,
                dgMaschine.Columns[e.ColumnIndex].Name, dgMaschine[e.ColumnIndex, e.RowIndex].Value.ToString());
            MaschinenMessage.Text = "Änderung erfolgreich";
            InitDataGridViews();
        }

        private void btNewMaschine_Click(object sender, EventArgs e)
        {
            Form addMaschine = new NewMaschineScreen(MaschinTable.InsertData, database);
            addMaschine.Show();
        }

        private void allocationPage_Paint(object sender, PaintEventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form allocation = new NewAllocationScreen(AllocationTable.InsertData,
                database.CommandSelectAsListFrom("iButtonID", "ibutton", ""),
                database.CommandSelectAsListFrom("MaschinenID", "maschine", ""), database);
            allocation.Show();
        }

        private void btStartSimulation_Click(object sender, EventArgs e)
        {
            if (cbMaschine.Text == "")
            {
                lSimulationsResult.Text = "Maschine Auswählen";
                return;
            }

            lSimulationsResult.Text = "Warte auf Button";
            Maschine = new Machine(cbMaschine.Text, database);
            KeyReader reader = new KeyReader();
            reader.ReadID += button.read_IDs;
            reader.OnKeyRead += startMaschine;
            reader.ReadKey();
        }

        private void startMaschine(string buttonid)
        {
            buttonid = buttonid.Split(';')[0];
            bool status;
            if (isRunning)
            {
                status = Maschine.StopMaschine(buttonid);
                if (status)
                {
                    //lSimulationsResult.Text = "Maschine wurde wieder gestopt";
                    lSimulationsResult.Invoke(new Action(() =>
                    {
                        lSimulationsResult.Text = "Maschine wurde wieder gestopt";
                    }));
                    isRunning = false;
                }
                else
                {
                    //lSimulationsResult.Text = "Maschine läuft weiter: Flascher button";
                    lSimulationsResult.Invoke(new Action(() =>
                    {
                        lSimulationsResult.Text = "Maschine läuft weiter: Flascher button";
                    }));
                }

                return;
            }

            status = Maschine.StartMaschine(buttonid);
            if (!status)
            {
                //lSimulationsResult.Text = "Button hat keinen zugriff auf diese Maschine";
                lSimulationsResult.Invoke(new Action(() =>
                {
                    lSimulationsResult.Text = "Button hat keinen zugriff auf diese Maschine";
                }));
            }
            else
            {
                //lSimulationsResult.Text = "Berechtigung erteilt, Maschine wurde gestartet";
                lSimulationsResult.Invoke(new Action(() =>
                {
                    lSimulationsResult.Text = "Berechtigung erteilt, Maschine wurde gestartet";
                }));
                isRunning = true;
            }
        }

        private void simulationPage_Paint(object sender, PaintEventArgs e)
        {
        }

        private void dgAllocation_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //AllocationTable.UpdateLine(dgAllocation[0, e.RowIndex].Value.ToString(),dgAllocation.Columns[0].Name, dgAllocation.Columns[e.ColumnIndex].Name, dgAllocation[e.ColumnIndex, e.RowIndex].Value.ToString());
            //muss noch gemacht werden
            InitDataGridViews();
        }

        private void btDeleteUser_Click(object sender, EventArgs e)
        {
            var rows = dgUser.SelectedRows;
            for (int i = 0; i < rows.Count; i++)
            {
                UserTabel.DeleteData(dgUser.Columns[0].Name, dgUser[0, rows[i].Index].Value.ToString());
            }

            InitDataGridViews();
        }

        private void btDeleteMaschine_Click(object sender, EventArgs e)
        {
            var rows = dgMaschine.SelectedRows;
            for (int i = 0; i < rows.Count; i++)
            {
                MaschinTable.DeleteData(dgMaschine.Columns[0].Name, dgMaschine[0, rows[i].Index].Value.ToString());
            }

            InitDataGridViews();
        }

        private void btDeleteAllocation_Click(object sender, EventArgs e)
        {
            var rows = dgAllocation.SelectedRows;
            for (int i = 0; i < rows.Count; i++)
            {
                string condition = "";
                for (int j = 0; j < dgAllocation.Columns.Count - 1; j++)
                {
                    condition += dgAllocation.Columns[j].Name + " = '" +
                                 dgAllocation[j, rows[i].Index].Value.ToString() + "'";
                    if (j != dgAllocation.Columns.Count - 2)
                    {
                        condition += " and ";
                    }
                }

                AllocationTable.DeleteData(condition);
            }

            InitDataGridViews();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            InitDataGridViews();
        }
    }
}