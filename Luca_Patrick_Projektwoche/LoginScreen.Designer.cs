using System.ComponentModel;

namespace Luca_Patrick_Projektwoche
{
    partial class LoginScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbError = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.tbPasswort = new System.Windows.Forms.TextBox();
            this.btLogin = new System.Windows.Forms.Button();
            this.btAbbruch = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbError
            // 
            this.lbError.Location = new System.Drawing.Point(82, 4);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(156, 28);
            this.lbError.TabIndex = 0;
            this.lbError.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbName
            // 
            this.lbName.Location = new System.Drawing.Point(79, 48);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(174, 16);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "Benutzername";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(79, 67);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(175, 20);
            this.tbName.TabIndex = 2;
            // 
            // lbPassword
            // 
            this.lbPassword.Location = new System.Drawing.Point(77, 90);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(183, 18);
            this.lbPassword.TabIndex = 3;
            this.lbPassword.Text = "Passwort";
            // 
            // tbPasswort
            // 
            this.tbPasswort.Location = new System.Drawing.Point(77, 107);
            this.tbPasswort.Name = "tbPasswort";
            this.tbPasswort.Size = new System.Drawing.Size(176, 20);
            this.tbPasswort.TabIndex = 4;
            this.tbPasswort.UseSystemPasswordChar = true;
            // 
            // btLogin
            // 
            this.btLogin.Location = new System.Drawing.Point(78, 133);
            this.btLogin.Name = "btLogin";
            this.btLogin.Size = new System.Drawing.Size(169, 21);
            this.btLogin.TabIndex = 5;
            this.btLogin.Text = "Login";
            this.btLogin.UseVisualStyleBackColor = true;
            this.btLogin.Click += new System.EventHandler(this.btLogin_Click_1);
            // 
            // btAbbruch
            // 
            this.btAbbruch.Location = new System.Drawing.Point(79, 159);
            this.btAbbruch.Name = "btAbbruch";
            this.btAbbruch.Size = new System.Drawing.Size(169, 21);
            this.btAbbruch.TabIndex = 6;
            this.btAbbruch.Text = "Abbruch";
            this.btAbbruch.UseVisualStyleBackColor = true;
            this.btAbbruch.Click += new System.EventHandler(this.btAbbruch_Click_1);
            // 
            // LoginScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 207);
            this.ControlBox = false;
            this.Controls.Add(this.btAbbruch);
            this.Controls.Add(this.btLogin);
            this.Controls.Add(this.tbPasswort);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lbError);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LoginScreen";
            this.Text = "LoginScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAbbruch;
        private System.Windows.Forms.Button btLogin;
        private System.Windows.Forms.TextBox tbPasswort;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbError;
    }
}